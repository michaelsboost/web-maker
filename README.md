Web-Maker
======

A fork of web-maker to see your code updates fullscreen on a mobile device;

Follow [@webmakerApp](https://twitter.com/intent/follow?screen_name=webmakerApp) for updates or tweet out feature requests and suggestions.

### Support Web Maker

Web Maker is completely free and open-source. If you find it useful, you can show your support by sharing it in your social network or by donating on [Gratipay](https://gratipay.com/Web-Maker/) or by simply letting me know how much you  it by tweeting to [@webmakerapp](https://twitter.com/webmakerApp).

### License

MIT Licensed

Copyright (c) 2017 Kushagra Gour, [webmakerapp.com](https://webmakerapp.com)
